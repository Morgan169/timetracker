## Synopsis

A tool to track the active time of an Executable. 

- Start and the tool will automatically detect when you start the application you want to timetrack
- Closing the application ends the current session, reopening creates a new session
- Configure the time between updates, the application you want to track and the time you already spent via an ini file

## Motivation

The Star Citizen Launcher currently doesn't keep track of the time played, so I made an easy-to-use tool to do that, until this feature is implemented. However, the tool can track any Executable that shows up in the Task Manager. 

## Downloads

You can download the latest release [here.](https://gitlab.com/Morgan169/timetracker/tags/v0.1)